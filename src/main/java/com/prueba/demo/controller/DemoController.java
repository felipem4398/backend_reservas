package com.prueba.demo.controller;

import java.util.concurrent.atomic.AtomicLong;

import com.prueba.demo.entity.Greetting;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DemoController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greetting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greetting(counter.incrementAndGet(), String.format(template, name));
    }
}